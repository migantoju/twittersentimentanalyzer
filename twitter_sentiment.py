#!/usr/bin/python3
# Author Miguel Toledano
import re
import os
import tweepy
from tweepy import OAuthHandler
from textblob import TextBlob
import pandas as pd
import matplotlib.pyplot as plt

# PDF
from fpdf import FPDF

class TwitterClient(object):

    def __init__(self):

        CONSUMER_KEY = ""
        CONSUMER_SECRET = ""
        ACCESS_TOKEN = ""
        ACCESS_SECRET = ""

        try:
            self.auth = OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
            self.auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)
            self.api = tweepy.API(self.auth)

        except:
            print("Error: Authentication failed")

    def clean_tweet(self, tweet):
        return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ", tweet).split())

    def get_tweet_sentiment(self, tweet):

        analysis = TextBlob(self.clean_tweet(tweet))
        if analysis.sentiment.polarity > 0:
            return 'positive'
        elif analysis.sentiment.polarity == 0:
            return 'neutral'
        else:
            return 'negative'

    def get_tweets(self, query, count = 20):

        tweets = []

        try:
            fetched_tweets = self.api.search(q = query, count = count)

            for tweet in fetched_tweets:
                parsed_tweet = {}

                parsed_tweet['text'] = tweet.text
                parsed_tweet['sentiment'] = self.get_tweet_sentiment(tweet.text)

                if tweet.retweet_count > 0:
                    if parsed_tweet not in tweets:
                        tweets.append(parsed_tweet)
                else:
                    tweets.append(parsed_tweet)
            return tweets
        except tweepy.TweepError as e:
            print("Error : " + str(e))

def main():

    text = str(input("�Sobre que tema desea buscar?: \n\n"))
    max_tweets = int(input("Cantidad de tweets que desea analizar: \n\n"))

    api = TwitterClient()
    tweets = api.get_tweets(query=text, count=max_tweets)

    ptweets = [tweet for tweet in tweets if tweet['sentiment'] == 'positive']
    print("Porcentaje de tweets positivos: {} %".format(100*len(ptweets)/len(tweets)))

    ntweets = [tweet for tweet in tweets if tweet['sentiment'] == 'negative']
    print("Porcentaje de tweets negativo: {} %".format(100*len(ntweets)/len(tweets)))

    neutral = len(tweets) - len(ntweets)
    neutral2 = neutral - len(ptweets)

    print("Tweets neutrales: {}".format(neutral2))

    #print("Porcentaje de tweets neutrales: {} %".format(100*len(tweets - ntweets - ptweets)/len(tweets)))

    print("\n\nTweets Positivos: ")
    for tweet in ptweets[:10]:
        print(tweet['text'])

    print("\n\nTweets Negativos")
    for tweet in ntweets[:10]:
        print(tweet['text'])

    ## PARTE DE GRAFICA PARA MOSTRAR EL %
    labels = ['Positivo', 'Negativo', 'Neutral']
    values = [len(ptweets), len(ntweets), neutral]
    colors = ['lightcoral', 'lightskyblue', '#2ec997']
    explode = (0.1, 0, 0)

    plt.title('Sentimiento de las personas en Twitter sobre el tema: {}'.format(text))
    plt.pie(values, explode=explode, labels=labels, colors=colors, autopct='%1.1f%%', shadow=True, startangle=140)
    plt.axis('equal')
    plt.savefig('result.pdf')
    plt.show()
    ##PDF
    # asd = '\n'.join(str(x) for x in ptweets)
    # pdf = FPDF()
    # pdf.add_page()
    # pdf.set_font('Helvetica', '', size=12)
    # pdf.cell(200, 10, txt=str(ptweets), ln=1, align='C')
    # pdf.output("test1.pdf")
    
if __name__ == '__main__':
    main()
