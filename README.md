# Twitter Sentiment Analyzer

Twitter Sentiment Analyzer es un script sencillo escrito en Python (v3), lo que hace es analizar tweets con la API oficial de Twitter sobre un tema en especifico y una cantidad de tweets que se define por el usuario. Una vez definido el tema y la cantidad, se generará una gráfica de tipo Pastel con el sentimiento de los usuarios acerca de ese tema (%), contando con 3 estados: Positivo, Negativo y Neutral.

 ![Imagen de Ejemplo](https://imgur.com/a/718TKdP)

# Instalación
Antes que nada clonamos el repositorio `git clone https://migantoju@bitbucket.org/migantoju/twittersentimentanalyzer.git`
Para hacer uso del script, es necesario contar con API Keys de Twitter, las puedes conseguir [aquí](https://developer.twitter.com/apps). Una vez que tengas tus llaves, se procede a instalar las dependencias del archivo `requirements.txt`
Ahora solo editamos el apartado

    CONSUMER_KEY = "*****************"
    CONSUMER_SECRET = "********************"
    ACCESS_TOKEN = "***********************"
    ACCESS_SECRET = "**********************"
Con nuestras llaves, corremos el script y ya está listo, ahora podemos analizar como se sienten las personas en twitter con base a una tema elegido.

Antes de poder utilizar el script, es necesario instalar las dependencias que vienene en archivo *requirements.txt* con el siguiente comando *pip install -r requeriments.txt* y así, ya tendremos instaladas las dependencias listas para correr el Script y que nos nos de ningún error.

## Autores

 - Miguel Toledano - [Twitter](https://twitter.com/migantoju0) [Facebook](https://facebook.com/migantoju) [instagram](https://instagram.com/migantoju) [Bitbucket](https://bitbucket.com/migantoju)
